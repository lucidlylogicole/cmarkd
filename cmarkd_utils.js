
function textTable2md(text, options) {
    // Convert 
    if (options === undefined) {options = {}}
    if (options.delimeter === undefined){options.delimeter='\t'}
    if (options.headers === undefined){options.headers=[]}
    
    // Parse table text
    let table = []
    let lengths = []
    for (let ln of text.trim().split('\n')) {
        let rw = []
        let c = -1
        for (let cell of ln.split(options.delimeter)) {
            c++
            let cell_text = cell.trim()
            if (cell_text == '') {cell_text = '.'}
            rw.push(cell_text)

            // get text length
            if (lengths[c] == undefined) {lengths[c]=0}
            if (cell_text.length > lengths[c]) {
                lengths[c] = cell_text.length
            }
        }
        
        table.push(rw)
        
    }
    
    // Find Row Length
    let row_length = 1
    for (let length of lengths) {row_length += length+2}

    // Build Markdown table text
    let md_text = ''
    // for (let rw of table) {
    for (let r=0; r < table.length; r++) {
        if (md_text != '') {md_text += '\n'}
        if (options.headers.indexOf(r) > -1) {
            md_text += '|'+''.padEnd(row_length,'-')+'\n'
        }
        md_text += '| '
        for (let c=0; c<lengths.length; c++) {
            if (table[r][c] == undefined){table[r][c]='.'} // make sure 
            md_text += table[r][c].padEnd(lengths[c]+2,' ')
        }
    }
    
    return md_text
}

//---exports
if (module !== undefined && module.exports !== undefined) {
    module.exports.textTable2md = textTable2md
}