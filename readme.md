# cmarkd.js

A JavaScript library that extends (and embeds) [commonmark.js](https://github.com/commonmark/commonmark.js) with additional syntax parsing and output tools.  

[Commonmark](https://commonmark.org/) is a nice specification of Markdown but is missing a few things that cmarkd has included.

## Features
- single JavaScript file with embedded commonmark.js
- can be used within a browser or with Nodejs
- additional checklist syntax
- additional table syntax
- optional document style (`cmarkd.min.css` and `cmarkd-dark.min.css` dark theme)
- file and document generation

## Installation
- just grab the [cmarkd.js](https://gitlab.com/lucidlylogicole/cmarkd/-/raw/master/cmarkd.js) file

----

## cmarkd.js

**cmarkdRender(txt, options)** - returns rendered html (string) from the markdown text
- `txt` - (string) markdown text to parse
- `options` - (dictionary) output options
    - `render_checklist` (bool) - render the checklist syntax (default is true)
    - `render_table` (bool) - render the table syntax (default is true)
    - `render_table_cells` (bool) - render each table cell as markdown (default is true)
        - for large tables or when this is not necessary, setting this to false will speed up the parsing
    - `commonmark_parser` (dict) options passed to the `commonmark.Parser`
    - `commonmark_renderer` (dict) options passed to the `commonmark.HtmlRenderer`

**cmarkdDocument(txt, options)** - returns a fully rendered html document (string) with the cmarkd style from markdown text
- `txt` - (string) markdown text to parse
- `options` - (dictionary) same options as cmarkRender plus the following:
    - `style` - (string) add custom CSS style that is appended after the default style
- note: the first `# ` heading is used as the document title

**cmarkdStyle()** - returns a string of the cmarkd style (minified)

----

## Checklist Syntax

    - [ ] not done
    - [x] done
    - [-] cancelled
    - [f] a future task

## Table Syntax

Rules
- tables start with a pipe `|`
- columns are delimited by 2 or more spaces (including tabs) or 1 or more tabs
    - the columns don't have to line up
- for blank cells, use a period `.`
- a heading row is determined if the following line starts with  `|-`
- by default, each cell is parsed individually for markdown, but can be ignored by setting the option `render_table_cells:false`

### Example Table

    | col A     col B
    |--------------------
    | 1         2
    | .         *italic*


----

## Browser Use

    <script src="cmarkd.js"></script>
    let md_text = '# Heading'
    let html = cmarkdRender(md_text)

## Nodejs Module Use

    const {cmarkdRender} = require('./cmarkd.js')
    let md_text = '# Heading'
    let html = cmarkdRender(md_text)

## Nodejs Commandline Use

    node cmarkd.js <markdownfile> -f <outputfile> -d

- `node` - node executable
- `cmarkd.js` - reference the cmarkd.js file
- `<markdownfile>` - filename of the markdown file to parse
- `-f` - (optional) - output to a file (if left off, the output will be to stdout)
- `<outputfile>` (optional) optionally specify the output file
    - This argument must follow the `-f` argument
    - if not specified then a file with the same name but with the `html` extension will be generated
- `-d` (optional) output a full html document with the cmarkd style.

### Commandline Examples

    // Output rendered html as stdout
    node cmarkd.js readme.md

    // Output to a file (readme.html)
    node cmarkd.js readme.md -f

    // Output to a file and specify the filename
    node cmarkd.js readme.md -f readme2.html

    // Output a full html document with cmarkd styling
    node cmarkd.js readme.md -f readme2.html -d

----

## A Note on Security
from commonmark.js:

> The library does not attempt to sanitize link attributes or raw HTML. If you use this library in applications that accept untrusted user input, you should either enable the safe option (see commonmarkjs readme) or run the output through an HTML sanitizer to protect against XSS attacks.

----

## cmarkd_utils.js
The cmarkd_utils module has some extra functions that may be helpful for converting to and from markdown.

**texttable2md(text, options)** - returns the cmarkd table format markdown from a text delimited table
- `text` - (string) table text
- `options` - (dict) table options
    - `delimeter` (string) - the column delimeter (default is `\t`)
    - `headers` (array) - where the table header indicator should be.  If the first row is a header then `headers:[1]`

Table text to markdown table Example

    const {textTable2md} = require('./cmarkd_utils.js')
    let text = fs.readFileSync(filename,'utf8')
    console.log(textTable2md(text,{headers:[1]}))